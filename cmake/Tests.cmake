macro(forked_test)
    file(GLOB TESTING_CXX_SOURCES ./*.cpp)
    file(GLOB TESTING_HEADERS ./*.hpp)

    get_filename_component(TEST_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
    set(TEST_NAME "test_${TEST_NAME}")

    add_executable(${TEST_NAME} ${TESTING_CXX_SOURCES} ${TESTING_HEADERS})
    if (FORKED_TRACE)
        target_compile_options(${TEST_NAME} PUBLIC -fno-limit-debug-info)
    endif ()
    target_link_libraries(${TEST_NAME} forked wheels)

endmacro()

macro(test_library LIB_NAME)
    set(TASK_DIR ${CMAKE_CURRENT_SOURCE_DIR})

    file(GLOB_RECURSE LIB_CXX_SOURCES ${TASK_DIR}/${LIB_NAME}/*.cpp)
    file(GLOB_RECURSE LIB_HEADERS ${TASK_DIR}/${LIB_NAME}/*.hpp)

    add_library(${LIB_NAME} STATIC ${LIB_CXX_SOURCES} ${LIB_HEADERS})
    get_filename_component(LIB_PATH "." ABSOLUTE)
    target_include_directories(${LIB_NAME} PUBLIC .)
    target_link_libraries(${LIB_NAME} PUBLIC ${ARGN})
endmacro()

macro(forked_lib_test)
    get_filename_component(TEST_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
    set(TEST_NAME "test_${TEST_NAME}")

    add_executable(${TEST_NAME} test.cpp)
    target_link_libraries(${TEST_NAME} forked wheels ${ARGN})
endmacro()
