#include <forked/wrappers/atomic.hpp>

#include <forked/test/checker.hpp>

namespace forked {

static UInt bound = 0;

CheckerView& CheckerView::SetUIntAtomicBound(UInt value) {
  bound = value;
  return *this;
}

template <>
UInt CutValue(UInt value) {
  if (bound == 0) {
    return value;
  }
  return (value + bound) % bound;
}

}  // namespace forked