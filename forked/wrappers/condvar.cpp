#include <forked/wrappers/condvar.hpp>

#include <forked/core/checker.hpp>
#include <forked/core/wait_queue.hpp>

#include <forked/test/random.hpp>
#include <forked/test/fork.hpp>
#include <forked/test/trace.hpp>

namespace forked {

/////////////////////////////////////////////////////////////////////

class ConditionVariable::Impl {
 public:
  void Wait(Lock& lock) {
    // Spurious wakeup
    if (!GetCurrentChecker()->ForksDisabled() && Either()) {
      FORKED_INTERNAL_NOTE("Spurious wakeup in condvar");
      return;
    }

    // ShowNote("Going to park in condvar");

    Fork();
    ATOMICALLY {
      lock.unlock();
    }
    GetCurrentFiber()->Where() = "condvar";
    wait_queue_.Park();

    lock.lock();

    FORKED_INTERNAL_NOTE("Exited wait in condvar");
  }

  void NotifyOne() {
    Fork();
    wait_queue_.WakeOne();
  }

  void NotifyAll() {
    Fork();
    wait_queue_.WakeAll();
  }

 private:
  WaitQueue wait_queue_;
};

/////////////////////////////////////////////////////////////////////

ConditionVariable::ConditionVariable()
    : pimpl_(std::make_unique<ConditionVariable::Impl>()) {
}

ConditionVariable::~ConditionVariable() {
}

void ConditionVariable::wait(Lock& lock) {  // NOLINT
  pimpl_->Wait(lock);
}

void ConditionVariable::notify_one() {  // NOLINT
  pimpl_->NotifyOne();
}

void ConditionVariable::notify_all() {  // NOLINT
  pimpl_->NotifyAll();
}

}  // namespace forked
