#pragma once

#include <memory>

namespace forked {

class Mutex {
 public:
  Mutex();
  ~Mutex();

  // std::mutex-like / Lockable
  void lock();      // NOLINT
  bool try_lock();  // NOLINT
  void unlock();    // NOLINT

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace forked
