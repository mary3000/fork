#pragma once

#include <forked/test/detail.hpp>

#include <memory>
#include <functional>

namespace forked {

class Thread {
 public:
  Thread(FiberRoutine routine);

  Thread(Thread&& other);
  Thread& operator=(Thread&& other);

  ~Thread();

  void join();  // NOLINT

  bool joinable();  // NOLINT

  void detach();  // NOLINT

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace forked
