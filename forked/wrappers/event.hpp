#pragma once

#include <forked/core/wait_queue.hpp>
#include <forked/core/checker.hpp>

#include <forked/test/trace.hpp>

namespace forked {

class Event {
 public:
  void Await() {
    if (!ready_) {
      GetCurrentFiber()->Where() = "event awaiting";
      wait_queue_.Park();
    }
  }

  void Signal() {
    ready_ = true;
    if (!wait_queue_.IsEmpty()) {
      FORKED_INTERNAL_NOTE("Event signalled");
      wait_queue_.WakeAll();
    }
  }

 private:
  bool ready_{false};
  WaitQueue wait_queue_;
};

}  // namespace forked
