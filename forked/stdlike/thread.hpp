#pragma once

#include <forked/wrappers/thread.hpp>

#include <forked/core/fiber.hpp>

namespace forked {
namespace stdlike {

using thread = forked::Thread;  // NOLINT

namespace this_thread {

inline void yield() {  // NOLINT
  Yield();
}

}  // namespace this_thread

}  // namespace stdlike
}  // namespace forked
