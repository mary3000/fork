#pragma once

#include <forked/wrappers/atomic.hpp>

namespace forked {
namespace stdlike {

template <typename T>
using atomic = forked::Atomic<T>;  // NOLINT

}  // namespace stdlike
}  // namespace forked
