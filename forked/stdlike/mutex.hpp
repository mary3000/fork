#pragma once

#include <forked/wrappers/mutex.hpp>

namespace forked {
namespace stdlike {

using mutex = forked::Mutex;  // NOLINT

}  // namespace stdlike
}  // namespace forked
