#pragma once

#include <forked/wrappers/condvar.hpp>

namespace forked {
namespace stdlike {

using condition_variable = forked::ConditionVariable;  // NOLINT

}  // namespace stdlike
}  // namespace forked
