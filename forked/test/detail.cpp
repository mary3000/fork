#include <forked/test/detail.hpp>
#include <forked/test/fork.hpp>
#include <forked/test/random.hpp>

#include <forked/core/checker.hpp>

#include <string>

namespace forked {

/////////////////////////////////////////////////////////////////////

#if defined(FORKED_TRACE)

void ShowNote(const std::string& note, bool internal) {
  if (!IsRunningInChecker() ||
      (internal && GetCurrentChecker()->ForksDisabled())) {
    return;
  }
  GetCurrentChecker()->GetTracer().ShowNote(note);
}

void ShowLocalState() {
  if (!IsRunningInChecker() || GetCurrentChecker()->ForksDisabled()) {
    return;
  }
  GetCurrentChecker()->GetTracer().ShowLocalState();
}

void ShowState() {
  if (!IsRunningInChecker() || GetCurrentChecker()->ForksDisabled()) {
    return;
  }
  GetCurrentChecker()->GetTracer().ShowState();
}

#else

void ShowNote(const std::string& /*note*/) {
}

void ShowLocalState() {
}

void ShowState() {
}

#endif

/////////////////////////////////////////////////////////////////////

size_t Random(size_t max) {
  return forked::GetCurrentChecker()->Random(max);
}

bool Either() {
  return Random(1) == 1;
}

/////////////////////////////////////////////////////////////////////

void RunChecker(FiberRoutine main) {
  Checker checker;
  checker.Run(&main);
}

void OnFailure(const std::string& s) {
  forked::GetCurrentChecker()->OnFailure(s);
}

}  // namespace forked
