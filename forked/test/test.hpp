#pragma once

#include <forked/test/detail.hpp>
#include <forked/test/fork.hpp>

#include <sstream>

namespace forked {

////////////////////////////////////////////////////////////////////////////////

#define FORKED_TEST(name)                     \
  void TestBody();                          \
  void ForkTest() {                         \
    forked::Trace(forked::GetTracePath(#name)); \
    TestBody();                             \
  }                                         \
  int main() {                              \
    forked::RunChecker(ForkTest);             \
    return 0;                               \
  }                                         \
  void TestBody()

////////////////////////////////////////////////////////////////////////////////

#define FORKED_ASSERT(condition, message) \
  do {                                  \
    forked::Fork();                       \
    if (!(condition)) {                 \
      std::stringstream s;              \
      s << message;                     \
      forked::OnFailure(s.str());         \
    }                                   \
  } while (false)

//////////////////////////////////////////////////////////////////////

}  // namespace forked
