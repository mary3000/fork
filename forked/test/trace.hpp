#pragma once

#include <forked/core/trace/description.hpp>

#include <wheels/core/map.hpp>

#include <forked/test/detail.hpp>

namespace forked {

////////////////////////////////////////////////////////////////////////////////

#if defined(FORKED_TRACE)

#define FORKED_NOTE(note)        \
  do {                         \
    std::stringstream __s;     \
    __s << note;               \
    forked::ShowNote(__s.str()); \
  } while (false)

#define FORKED_INTERNAL_NOTE(note)     \
  do {                               \
    std::stringstream __s;           \
    __s << note;                     \
    forked::ShowNote(__s.str(), true); \
  } while (false)

#else

#define FORKED_NOTE(note)

#define FORKED_INTERNAL_NOTE(note)

#endif

void ShowLocalState();

void ShowState();

////////////////////////////////////////////////////////////////////////////////

// Automatic generation of a class description
// Usage: class MyClass {
// ...
// FORKED_DESCRIBE(field1, field2, field3)
// ...
// Type1 field1;
// Type2 field2;
// Type3 field3;
// ...
// }
#define FORKED_DESCRIBE(...)                        \
  trace::StateDescription DescribeState() const { \
    trace::StateDescription d;                    \
    MAP(FORKED_DESCRIBE_ITEM, __VA_ARGS__)          \
    return d;                                     \
  }

#define FORKED_DESCRIBE_ITEM(x) d.Add(#x, x);

////////////////////////////////////////////////////////////////////////////////

}  // namespace forked
