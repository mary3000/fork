#include <forked/core/wait_queue.hpp>

#include <forked/core/fiber.hpp>
#include <forked/core/checker.hpp>

#include <forked/test/random.hpp>
#include <forked/test/trace.hpp>

namespace forked {

static inline void Suspend(FiberDescriptor ptr) {
  GetCurrentChecker()->Suspend(ptr);
}

static inline void Resume(Fiber* f) {
  GetCurrentChecker()->Resume(f);
}

void WaitQueue::Park() {
  Suspend(this_);
  FORKED_INTERNAL_NOTE("Was woken up by thread "
                     << GetCurrentFiber()->Initiator());
}

void WaitQueue::WakeOne() {
  auto fibers = GetSuspendedFibers();
  if (fibers.empty()) {
    return;
  }
  size_t index = Random(fibers.size() - 1);
  fibers[index]->Initiator() = GetFiberId();
  Resume(fibers[index]);

  FORKED_INTERNAL_NOTE("Wake thread " << fibers[index]->Id() << " ("
                                    << SHOW_QUEUE(this_) << ")");
}

void WaitQueue::WakeAll() {
  FORKED_INTERNAL_NOTE("Wake all threads from " << SHOW_QUEUE(this_));

  auto fibers = GetSuspendedFibers();
  for (auto f : fibers) {
    f->Initiator() = GetFiberId();
    Resume(f);
  }
}

bool WaitQueue::IsEmpty() const {
  return GetSuspendedFibers().empty();
}

FiberPtrs WaitQueue::GetSuspendedFibers() const {
  return GetCurrentChecker()->GetSuspendedFibers(this_);
}

}  // namespace forked
