#include <wheels/logging/logging.hpp>
#include <forked/core/checker.hpp>

#include <new>

using namespace forked;

void* operator new(size_t size) {
  if (IsRunningInChecker()) {
    return GetCurrentChecker()->Allocate(size);
  }

  void* pointer = std::malloc(size);
  if (pointer) {
    return pointer;
  } else {
    throw std::bad_alloc{};
  }
}

void operator delete(void* pointer) noexcept {
  if (IsRunningInChecker()) {
    GetCurrentChecker()->Free(pointer);
    return;
  }

  std::free(pointer);
}

void* operator new[](std::size_t size) {
  if (IsRunningInChecker()) {
    return GetCurrentChecker()->Allocate(size);
  }

  void* pointer = std::malloc(size);
  if (pointer) {
    return pointer;
  } else {
    throw std::bad_alloc{};
  }
}

void operator delete[](void* pointer) throw() {
  if (IsRunningInChecker()) {
    GetCurrentChecker()->Free(pointer);
    return;
  }

  std::free(pointer);
}
