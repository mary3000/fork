#pragma once

#include <sure/context.hpp>
#include <forked/core/fiber.hpp>

namespace forked {

class UnpackedState {
 public:
 private:
  sure::ExecutionContext loop_context_;

  FiberPtrs fibers_;
  size_t existing_fiber_count_{0};
};

}  // namespace forked
