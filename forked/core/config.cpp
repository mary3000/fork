#include <forked/test/fork.hpp>

#include <forked/core/config.hpp>
#include <forked/core/exception.hpp>
#include <forked/core/checker.hpp>

namespace forked {

void Config::Run() {
  forked::ForkGuard guard;

  if (state_ != None) {
    return;
  }
  if (CheckInvariants()) {
    state_ = Failed;
  } else if (RunPrunes()) {
    state_ = Pruned;
  }
}

bool Config::CheckInvariants() {
  for (auto& check : invariants_) {
    auto [success, message] = check.predicate_();
    if (!success) {
      message_ = message;
      if (!fails_expected_ && !check.fail_expected_) {
        try {
          throw Unexpected(message);
        } catch (...) {
          exception_ = std::current_exception();
        }
      }
      return true;
    }
  }
  return false;
}

bool Config::RunPrunes() {
  for (auto& prune : prunes_) {
    if (prune()) {
      return true;
    }
  }
  return false;
}

void Config::OnFailure(const std::string& message) {
  AllocationGuard guard;

  state_ = Failed;
  message_ = message;

  if (fails_expected_) {
    return;
  }

  try {
    throw Unexpected(message);
  } catch (...) {
    exception_ = std::current_exception();
  }
}

}  // namespace forked
