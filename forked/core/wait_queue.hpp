#pragma once

#include <forked/core/fiber.hpp>

#include <memory>

namespace forked {

class WaitQueue {
 public:
  void Park();

  void WakeOne();
  void WakeAll();

  bool IsEmpty() const;

 private:
  FiberPtrs GetSuspendedFibers() const;

 private:
  const FiberDescriptor this_ = reinterpret_cast<FiberDescriptor>(this);
};

}  // namespace forked
