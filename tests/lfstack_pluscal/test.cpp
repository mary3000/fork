#include <forked/stdlike/thread.hpp>

#include <forked/test/test.hpp>
#include <forked/test/checker.hpp>

#include <iostream>
#include <cmath>

#include "alloc_lfstack.hpp"
#include "memory.hpp"

using namespace forked::stdlike;

FORKED_TEST(ABA) {
  MemoryPool pool{3};
  LFAllocator stack{pool};

  forked::GetChecker()
      .PrintState([&stack, &pool]() {
        trace::StateDescription d{"LFAlloc"};
        d.Add("LFStack", stack);
        d.Add("Memory", pool);
        return d;
      })
      .AddInvariant([&stack]() -> std::pair<bool, std::string> {
        std::string label;
        auto check_list = [&](Node* node) {
          int i = 0;
          while (node != nullptr) {
            if (node->IsReleased()) {
              label = node->GetLabel();
              return false;
            }
            node = node->next_.load();
            ++i;
          }
          return true;
        };

        if (!check_list(stack.Top())) {
          return {false, "Head: node " + label + "from stack was deleted"};
        }

        if (!check_list(stack.TopPending())) {
          return {false, "Pending: assertion failed"};
        }

        return {true, ""};
      })
      .FailsExpected();

  auto worker = [&stack]() {
    while (true) {
      Node* node{nullptr};

      if (stack.Alloc(node)) {
        stack.Free(node);
      }
    }
  };

  std::vector<thread> threads;
  const static size_t kThreads = 4;
  for (size_t i = 0; i < kThreads; ++i) {
    threads.emplace_back(worker);
  }

  for (auto& th : threads) {
    th.join();
  }
}
