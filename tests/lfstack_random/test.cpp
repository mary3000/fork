#include <forked/stdlike/thread.hpp>

#include <forked/test/test.hpp>
#include <forked/test/random.hpp>
#include <forked/test/checker.hpp>

#include <iostream>
#include <cmath>

#include "alloc_lfstack.hpp"
#include "memory.hpp"

using namespace forked::stdlike;

FORKED_TEST(ABA) {
  MemoryPool pool{3};
  LFAllocator stack{pool};

  forked::GetChecker()
      .PrintState([&stack, &pool]() {
        trace::StateDescription d{"LFAlloc"};
        d.Add("LFStack", stack);
        d.Add("Memory", pool);
        return d;
      })
      .AddInvariant([&stack]() -> std::pair<bool, std::string> {
        auto* node = stack.Top();
        while (node != nullptr) {
          if (node->IsReleased()) {
            return {false, "Node from stack was released!"};
          }
          node = node->next_.load();
        }
        return {true, ""};
      })
      .FailsExpected();

  auto worker = [&stack, &pool]() {
    while (true) {
      size_t available = pool.ReleasedCount();
      int node_index = forked::Random(available) - 1;
      if (node_index == -1) {
        FORKED_NOTE("Alloc(): before");
        stack.Alloc();
        FORKED_NOTE("Alloc(): after");
      } else {
        FORKED_NOTE("Free(): before");
        Node* to_free = pool.Acquire(node_index);
        stack.Free(to_free);
        FORKED_NOTE("Free(): after");
      }
    }
  };

  std::vector<thread> threads;
  const static size_t kThreads = 3;
  for (size_t i = 0; i < kThreads; ++i) {
    threads.emplace_back(worker);
  }

  for (auto& th : threads) {
    th.join();
  }
}
