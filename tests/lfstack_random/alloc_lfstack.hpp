#include <forked/stdlike/atomic.hpp>
#include <forked/test/fork.hpp>

#include "memory.hpp"

using namespace forked::stdlike;

//////////////////////////////////////////////////////////////////

class LockFreeStack {
 public:
  void Push(Node* node) {
    Node* current_top = top_.load();
    do {
      node->next_.store(current_top);
    } while (!top_.compare_exchange_weak(current_top, node));
  }

  void Append(Node* head) {
    if (head == nullptr) {
      return;
    }

    Node* tail = head;
    Node* next = tail->next_.load();
    while (next != nullptr) {
      tail = next;
      next = tail->next_.load();
    }

    Node* prev_top = top_.load();
    do {
      tail->next_ = prev_top;
    } while (!top_.compare_exchange_weak(prev_top, head));
  }

  Node* Top() {
    return top_.load();
  }

  Node* Pop() {
    Node* curr_top = top_.load();
    Node* next_top;
    do {
      if (curr_top == nullptr) {
        break;
      }
      next_top = curr_top->next_.load();
    } while (!top_.compare_exchange_weak(curr_top, next_top));
    return curr_top;
  }

  bool TryClear(Node* expected) {
    Node* desired = nullptr;
    return top_.compare_exchange_strong(expected, desired);
  }

  trace::StateDescription DescribeState() const {
    trace::StateDescription d;
    auto head = top_.load();
    std::stringstream chain;
    while (head != nullptr) {
      chain << "-> " << head->GetLabel();
      head = head->next_;
    }
    d.Add("blocks", chain.str());
    return d;
  }

 private:
  atomic<Node*> top_{nullptr};
};

//////////////////////////////////////////////////////////////////

class LFAllocator {
 public:
  LFAllocator(MemoryPool& pool) {
    ATOMICALLY {
      for (auto& node : pool.GetNodes()) {
        node.Acquire();
        item_list_.Push(&node);
      }
    }
  }

  void Free(Node* node) __attribute__((noinline)) {
    if (allocation_count_.load() == 0) {
      item_list_.Push(node);
    } else {
      pending_list_.Push(node);
    }
  }

  bool Alloc() __attribute__((noinline)) {
    // auto keep_count = pending_count_.load();
    Node* pending_head = pending_list_.Top();

    if (allocation_count_.fetch_add(1) == 0) {
      if (pending_head != nullptr /*&& keep_count == pending_count_.load()*/
          && pending_list_.TryClear(pending_head)) {
        auto head = pending_head;

        pending_head = pending_head->next_.load();
        head->Release();

        item_list_.Append(pending_head);
        // pending_count_.fetch_add(1);
        allocation_count_.fetch_sub(1);

        return true;
      }
    }

    auto node = item_list_.Pop();
    if (node != nullptr) {
      node->Release();
    }

    allocation_count_.fetch_sub(1);

    return node != nullptr;
  }

  Node* Top() {
    return item_list_.Top();
  }

  Node* TopPending() {
    return item_list_.Top();
  }

  FORKED_DESCRIBE(item_list_, pending_list_, allocation_count_)

 private:
  LockFreeStack item_list_;
  LockFreeStack pending_list_;

  atomic<int> allocation_count_{0};
  // atomic<int> pending_count_{0};
};

/////////////////////////////////////////////////////////////////////
