#include <wheels/test/test_framework.hpp>

#include <forked/stdlike/mutex.hpp>
#include <forked/stdlike/thread.hpp>

#include <forked/wrappers/event.hpp>

#include <forked/test/test.hpp>

using namespace forked::stdlike;

size_t MultinomialCoefficient(const size_t n, size_t k) {
  size_t total = n * k;
  size_t result = 1;

  for (size_t i = 0; i < n; ++i) {
    for (size_t j = 1; j <= k; ++j) {
      result *= total--;
      result /= j;
    }
  }

  return result;
}

TEST_SUITE(Combinations) {
  SIMPLE_TEST(Yields) {
  static const size_t kThreads = 2;
  static const size_t kSteps = 3;
  static const size_t kPathLength = kSteps * kThreads;
  static const size_t kExpectedCombinations =
      MultinomialCoefficient(kThreads, kSteps);

  size_t combinations = 0;
  std::string first;
  std::string last;

  auto spawner = [&]() {
    size_t current_length = 0;
    char buffer[kPathLength];

    forked::Event event;

    auto step = [&](char ch) {
      buffer[current_length++] = ch;

      if (current_length == kPathLength) {
        if (first.empty()) {
          first = {buffer, kPathLength};
        }
        last = {buffer, kPathLength};

        ++combinations;
        event.Signal();
      }
    };

    auto fiber = [step](char id) {
      for (size_t i = 0; i < kSteps - 1; ++i) {
        step(id);
        this_thread::yield();
      }
      step(id);
    };

    std::vector<thread> threads;
    char id = 'A';
    for (size_t i = 0; i < kThreads; ++i) {
      threads.emplace_back([id, &fiber]() { fiber(id); });
      ++id;
    }

    event.Await();

    for (auto& th : threads) {
      th.join();
    }
  };

  forked::RunChecker(spawner);

  ASSERT_EQ(combinations, kExpectedCombinations);
  ASSERT_EQ(first, "AAABBB");
  ASSERT_EQ(last, "BBBAAA");
  }
}

RUN_ALL_TESTS()
