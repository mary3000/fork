#include <forked/stdlike/mutex.hpp>
#include <forked/stdlike/thread.hpp>

#include <forked/test/test.hpp>
#include <forked/test/checker.hpp>

#include <cmath>
#include <string>

#include "channel.hpp"

using namespace forked::stdlike;

FORKED_TEST(LostWakeup) {
  forked::GetChecker()
      .ExpectDeadlock()
      .ShowLocals(false);

  static const size_t kItems = 2;
  Channel<int> items;

  auto consume = [&]() { items.Take(); };
  std::vector<thread> consumers;
  for (size_t i = 0; i < kItems; ++i) {
    consumers.emplace_back(consume);
  }

  thread producer([&]() {
    for (size_t i = 0; i < kItems; ++i) {
      items.Put(42);
    }
  });

  producer.join();
  for (auto& consumer : consumers) {
    consumer.join();
  }
}
