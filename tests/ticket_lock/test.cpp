#include <iostream>

#include <forked/test/trace.hpp>

#include <forked/stdlike/thread.hpp>

#include <forked/test/test.hpp>
#include <forked/test/checker.hpp>

#include <cmath>
#include <string>

#include "ticket_lock.hpp"

using namespace forked::stdlike;

FORKED_TEST(EndlessLoop) {
  const static size_t kThreads = 3;
  TicketLock lock;
  bool in_critical_section = false;

  forked::GetChecker()
      .SetUIntAtomicBound(5)
      .PrintState(
      [&lock, &in_critical_section]() {
        trace::StateDescription d{"LockState"};
        d.Add("TicketLock", lock);
        d.Add("in_critical_section_", in_critical_section);
        return d;
      });

  auto routine = [&]() {
    while (true) {
      lock.Lock();
      FORKED_ASSERT(!std::exchange(in_critical_section, true),
                  "Mutual exclusion violated");
      FORKED_ASSERT(std::exchange(in_critical_section, false),
                  "Mutual exclusion violated");
      lock.Unlock();
    }
  };

  std::vector<thread> threads;
  for (size_t i = 0; i < kThreads; ++i) {
    threads.emplace_back(routine);
  }

  for (auto& th : threads) {
    th.join();
  }
}
