#include <forked/stdlike/mutex.hpp>

#include <forked/test/test.hpp>
#include <forked/test/trace.hpp>
#include <forked/test/checker.hpp>

#include <tinyfutures/executors/static_thread_pool.hpp>
#include <tinyfutures/executors/strand.hpp>

#include <cmath>
#include <string>

using namespace executors;

FORKED_TEST(Strand) {
  forked::GetChecker()
      .FailsExpected();

  auto tp = MakeStaticThreadPool(1);
  auto strand = MakeStrand(tp);

  size_t tasks = 0;
  strand->Execute([&]() { ++tasks; });
  strand->Execute([&]() { ++tasks; });

  tp->Join();

  FORKED_ASSERT(tasks == 2, "Expected 2 completed tasks, found " << tasks);
}
