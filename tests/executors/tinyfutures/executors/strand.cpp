#include <tinyfutures/executors/strand.hpp>
#include <tinyfutures/executors/helpers.hpp>
#include <tinyfutures/executors/queues.hpp>

#include <forked/stdlike/atomic.hpp>

#include <forked/test/fork.hpp>

// #define CORRECT

namespace executors {

using forked::ForkGuarded;

using namespace forked::stdlike;

class Spinlock {
 public:
  bool TryLock() {
    bool success = !locked_.exchange(true);
    if (success) {
      FORKED_NOTE("Strand: locked");
    } else {
      FORKED_NOTE("Strand: failed to lock");
    }
    return success;
  }

  void Unlock() {
    locked_.store(false);
    FORKED_NOTE("Strand: unlocked");
  }

 private:
  atomic<bool> locked_{false};
};

class Strand : public IExecutor, public std::enable_shared_from_this<Strand> {
 public:
  Strand(IExecutorPtr executor) : underlying_(executor) {
  }

  void Execute(Task&& task) override {
    tasks_->Put(std::move(task));
    FORKED_NOTE("Strand: task was put to queue");
    TrySchedule();
  }

  void WorkCreated() override {
    underlying_->WorkCreated();
  }

  void WorkCompleted() override {
    underlying_->WorkCompleted();
  }

 private:
  void TrySchedule() {
    if (lock_->TryLock()) {
      Schedule();
    }
  }

  void Schedule() {
#if defined(CORRECT)
    auto self = shared_from_this();
    underlying_->Execute([this, self]() {
      ExecuteBulk();
      lock_->Unlock();
      if (!tasks_->IsEmpty()) {
        TrySchedule();
      }
    });
#else
    auto self = shared_from_this();
    underlying_->Execute([this, self]() {
      auto tasks_to_execute = tasks_->TakeAll();
      if (tasks_to_execute.empty()) {
        FORKED_NOTE("Strand: empty queue");
        lock_->Unlock();
        return;
      }
      FORKED_NOTE("Strand: queue not empty");
      for (auto& task : tasks_to_execute) {
        SafelyExecuteHere(task);
      }
      Schedule();
    });
    FORKED_NOTE("Strand: scheduled");
#endif
  }

  void ExecuteBulk() {
    std::deque<Task> tasks;
    tasks = tasks_->TakeAll();
    for (auto& task : tasks) {
      SafelyExecuteHere(task);
    }
  }

 private:
  IExecutorPtr underlying_;
  ForkGuarded<MPSCLockFreeQueue<Task>> tasks_;
  ForkGuarded<Spinlock> lock_;
};

IExecutorPtr MakeStrand(IExecutorPtr executor) {
  return std::make_shared<Strand>(executor);
}

}  // namespace executors
