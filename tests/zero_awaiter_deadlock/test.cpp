#include <forked/stdlike/atomic.hpp>
#include <forked/stdlike/thread.hpp>
#include <forked/stdlike/condition_variable.hpp>
#include <forked/stdlike/mutex.hpp>

#include <forked/test/test.hpp>
#include <forked/test/checker.hpp>

#include <cmath>

using namespace std::literals::chrono_literals;

using namespace forked::stdlike;

class ZeroAwaiter {
 public:
  void Up() {
    tokens_.fetch_add(1);
  }

  void Down() {
    tokens_.fetch_sub(1);
    reached_zero_.notify_one();
  }

  void Await() {
    std::unique_lock lock(mutex_);
    while (tokens_.load() != 0) {
      reached_zero_.wait(lock);
    }
  }

 private:
  condition_variable reached_zero_;
  mutex mutex_;
  atomic<size_t> tokens_{0};
};

FORKED_TEST(LostWakeup) {
  ZeroAwaiter awaiter;

  forked::GetChecker()
      .ExpectDeadlock();

  thread producer([&awaiter]() {
    awaiter.Up();
    awaiter.Down();
  });

  thread waiter([&awaiter]() { awaiter.Await(); });

  waiter.join();
  producer.join();
}
