#include <forked/stdlike/thread.hpp>

#include <forked/stdlike/atomic.hpp>
#include <forked/test/test.hpp>
#include <forked/test/checker.hpp>

#include <cmath>
#include <string>
#include <iostream>

#include "ticket_lock.hpp"

using namespace forked::stdlike;

FORKED_TEST(AtomicOverflow) {
  TicketLock lock;
  bool in_critical_section = false;

  forked::GetChecker()
      .SetUIntAtomicBound(3)
      .FailsExpected()
      .PrintState(
      [&lock, &in_critical_section]() {
        trace::StateDescription d{"LockState"};
        d.Add("TicketLock", lock);
        d.Add("in_critical_section_", in_critical_section);
        return d;
      });

  auto routine = [&]() {
    while (true) {
      if (lock.TryLock()) {
        FORKED_ASSERT(!std::exchange(in_critical_section, true),
                    "Mutual exclusion violated");
        FORKED_ASSERT(std::exchange(in_critical_section, false),
                    "Mutual exclusion violated");
        lock.Unlock();
      }
    }
  };

  const static size_t kThreads = 2;
  std::vector<thread> threads;
  for (size_t i = 0; i < kThreads; ++i) {
    threads.emplace_back(routine);
  }

  for (auto& th : threads) {
    th.join();
  }
}
