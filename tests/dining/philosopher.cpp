#include "philosopher.hpp"

#include <forked/test/fork.hpp>

namespace dining {

void Philosopher::EatOneMoreTime() {
  AcquireForks();
  Eat();
  ReleaseForks();
  Think();
}

// Acquires left_forked_ and right_forked_
void Philosopher::AcquireForks() {
  left_forked_.lock();
  right_forked_.lock();
}

void Philosopher::Eat() {
  table_.AccessPlate(seat_);
  table_.AccessPlate(table_.ToRight(seat_));
  ++eat_count_;
}

// Releases left_forked_ and right_forked_
void Philosopher::ReleaseForks() {
  right_forked_.unlock();
  left_forked_.unlock();
}

void Philosopher::Think() {
  forked::Fork();
}

}  // namespace dining
