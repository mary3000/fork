#include "dining.hpp"

#include <forked/test/test.hpp>

namespace dining {

void Plate::Access() {
  FORKED_ASSERT(!accessed_.exchange(true, std::memory_order_relaxed),
                 "Mutual exclusion violated");
  FORKED_ASSERT(accessed_.exchange(false, std::memory_order_relaxed),
                "Mutual exclusion violated");
}

}  // namespace dining
