#pragma once

#include <forked/stdlike/mutex.hpp>

#include <atomic>
#include <vector>

namespace dining {

using Fork = forked::stdlike::mutex;

class Plate {
 public:
  void Access();

 private:
  std::atomic<bool> accessed_{false};
};

class Table {
 public:
  Table(size_t num_seats)
      : num_seats_(num_seats), plates_(num_seats_), forkeds_(num_seats_) {
  }

  Fork& LeftFork(size_t seat) {
    return forkeds_[seat];
  }

  Fork& RightFork(size_t seat) {
    return forkeds_[ToRight(seat)];
  }

  size_t ToRight(size_t seat) const {
    return (seat + 1) % num_seats_;
  }

  void AccessPlate(size_t seat) {
    plates_[seat].Access();
  }

 private:
  size_t num_seats_;
  std::vector<Plate> plates_;
  std::vector<Fork> forkeds_;
};

}  // namespace dining
