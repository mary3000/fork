#include <forked/stdlike/mutex.hpp>
#include <forked/stdlike/thread.hpp>

#include <forked/test/test.hpp>
#include <forked/test/trace.hpp>
#include <forked/test/checker.hpp>

#include <cmath>
#include <string>

#include "dining.hpp"
#include "philosopher.hpp"

using namespace std::literals::chrono_literals;

using namespace forked::stdlike;

FORKED_TEST(Dining) {
  forked::GetChecker()
      .ExpectDeadlock()
      .ShowLocals(false);

  static const size_t kSeats = 4;

  dining::Table table{kSeats};

  std::vector<thread> workers;
  for (size_t i = 0; i < kSeats; ++i) {
    workers.emplace_back([&, i]() {
      dining::Philosopher plato{table, i};
      while (true) {
        plato.EatOneMoreTime();
      }
    });
  }

  for (auto& w : workers) {
    w.join();
  }
}
