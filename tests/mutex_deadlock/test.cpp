#include <forked/test/test.hpp>
#include <forked/test/trace.hpp>
#include <forked/test/checker.hpp>

#include <forked/stdlike/thread.hpp>
#include <forked/stdlike/mutex.hpp>

using namespace forked::stdlike;

FORKED_TEST(Deadlock) {
  forked::GetChecker()
      .ExpectDeadlock()
      .ShowLocals(false);

  mutex first_mutex;
  mutex second_mutex;

  thread first_thread([&]() {
    first_mutex.lock();
    second_mutex.lock();

    second_mutex.unlock();
    first_mutex.unlock();
  });

  thread second_thread([&]() {
    second_mutex.lock();
    first_mutex.lock();

    first_mutex.unlock();
    second_mutex.unlock();
  });

  first_thread.join();
  second_thread.join();
}
