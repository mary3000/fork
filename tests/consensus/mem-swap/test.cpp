#include <forked/stdlike/mutex.hpp>
#include <forked/stdlike/thread.hpp>
#include <forked/stdlike/atomic.hpp>

#include <forked/test/test.hpp>
#include <forked/test/checker.hpp>
#include <forked/test/fork.hpp>

#include <cmath>
#include <string>

using namespace forked::stdlike;

template <typename T>
void MemSwap(atomic<T>& left, atomic<T>& right) {
  forked::Fork();
  forked::ForkGuard guard;
  T to_right = left.load();
  left.store(right.load());
  right.store(to_right);
}

FORKED_TEST(Memswap) {
  forked::GetChecker()
      .ShowLocals(false);
  // todo: forked::HashOff

  // Consensus objects

  static const size_t kThreads = 4;
  std::array<int, kThreads> generated;
  auto generator = std::hash<int>();
  for (size_t i = 0; i < kThreads; ++i) {
    generated[i] = generator(i);
    FORKED_NOTE("generated: " << generated[i]);
  }

  std::array<atomic<int>, kThreads> decisions;
  {
    forked::ForkGuard guard;
    for (auto& decision : decisions) {
      decision.store(-1);
    }
  }

  // User objects

  std::array<atomic<int>, kThreads> inputs;
  {
    forked::ForkGuard guard;
    for (auto& val : inputs) {
      val.store(-1);
    }
  }

  std::array<atomic<int>, kThreads> ids;
  {
    forked::ForkGuard guard;
    for (size_t i = 0; i < kThreads; ++i) {
      ids[i].store(i);
    }
  }

  atomic<int> rendezvous{-1};

  auto decide = [&](int value, size_t id) {
    inputs[id].store(value);
    FORKED_NOTE("value: " << value);

    MemSwap(ids[id], rendezvous);

    if (ids[id].load() == -1) {
      decisions[id].store(value);
      return;
    } else {
      for (size_t i = 0; i < kThreads; ++i) {
        if (ids[i].load() == -1) {
          decisions[id].store(inputs[i].load());
          return;
        }
      }
    }

    FORKED_ASSERT(false, "unreachable");
  };

  std::vector<thread> consumers;
  for (size_t i = 0; i < kThreads; ++i) {
    FORKED_NOTE("v0: " << generated[i]);
    consumers.emplace_back(std::bind(decide, generated[i], i));
  }

  for (auto& consumer : consumers) {
    consumer.join();
  }

  // Check for validity, agreement

  forked::ForkGuard guard;

  int first_decision = decisions[0].load();

  auto it = std::find(generated.begin(), generated.end(), first_decision);
  FORKED_ASSERT(it != generated.end(), "validity violated");

  for (auto& d : decisions) {
    FORKED_ASSERT(d.load() == first_decision, "agreement violated");
  }
}
