#include <iostream>

#include <forked/test/test.hpp>
#include <forked/test/checker.hpp>

#include <forked/stdlike/thread.hpp>

#include <forked/core/checker.hpp>

#include <cmath>

#include "buggy_lfstack.hpp"

using namespace forked::stdlike;

FORKED_TEST(PushPop) {
  static const int kExpected = 5;
  static const size_t kThreads = 3;

  forked::GetChecker()
      .FailsExpected()
      .ShowLocals(false);

  BuggyLockFreeStack<int> stack;

  auto worker = [&]() {
    while (true) {
      stack.Push(kExpected);

      int result;
      bool popped = stack.Pop(result);

      FORKED_ASSERT(popped, "empty stack");
      FORKED_ASSERT(result == kExpected, "unexpected popped value");
    }
  };

  std::vector<thread> threads;
  for (size_t i = 0; i < kThreads; ++i) {
    threads.emplace_back(worker);
  }

  for (auto& th : threads) {
    th.join();
  }
}
